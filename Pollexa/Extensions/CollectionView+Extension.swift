//
//  CollectionView+Extension.swift
//  Pollexa
//
//  Created by admin on 15.05.2024.
//

import UIKit

extension UICollectionView {
    func reloadAsync(){
        DispatchQueue.main.async { 
            self.reloadData()
        }
    }
}
