//
//  Date+Extension.swift
//  Pollexa
//
//  Created by admin on 15.05.2024.
//

import UIKit

extension DateFormatter {
    func formattedTimeAgo(from date: Date) -> String {
        let calendar = Calendar.current
        let now = Date()
        
        let components = calendar.dateComponents([.hour,.day, .weekOfYear,.year], from: date, to: now)
        if let years = components.year, years > 0 {
            return "\(years) years ago"
        }
        
        else if let weeks = components.weekOfYear, weeks > 0 {
            if weeks == 1 {
                return "Previous week"
            } else {
                return "\(weeks) weeks ago"
            }
        }
        
        else if let days = components.day, days > 0 {
                if days == 1 {
                    return "Yesterday"
                } else {
                    return "\(days) days ago"
                }
            }
        else if let hours = components.hour, hours > 0 {
            return "\(hours) hours ago"
        }
        
        else if let minutes = components.minute, minutes > 0{
            return "\(minutes) minutes ago"
        }
        
        return ""
    }
}

