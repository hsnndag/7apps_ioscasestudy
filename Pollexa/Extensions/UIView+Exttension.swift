//
//  UIView+Exttension.swift
//  Pollexa
//
//  Created by admin on 14.05.2024.
//

import UIKit

extension UIView {
    
    /// For get device screen scale
    static var WIDTH: CGFloat {
        return UIScreen.main.bounds.width
    }
    
    static var HEIGHT: CGFloat {
        return UIScreen.main.bounds.height
    }
    
    static var statusBarHeight: CGFloat {
        let scenes = UIApplication.shared.connectedScenes
        let windowScene = scenes.first as? UIWindowScene
        return windowScene?.statusBarManager?.statusBarFrame.height ?? 0
    }
}
