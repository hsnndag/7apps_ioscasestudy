//
//  LaunchViewModel.swift
//  Pollexa
//
//  Created by admin on 14.05.2024.
//

import Foundation

protocol LaunchViewModelProtocol: AnyObject {
    var view: LaunchViewProtocol? {get set}
    func viewDidLoad()
}
// Launch viewmodel normalde config vs ayarlayabilir ve bu durumlar test edilebilir fakat bu casede pek bi işlemi olmadığından loadview i tetikledim
final class LaunchViewModel: BaseViewModel, LaunchViewModelProtocol {
    
    weak var view: LaunchViewProtocol?
    
    func viewDidLoad() {
        view?.whenViewDidLoad()
    }
}
