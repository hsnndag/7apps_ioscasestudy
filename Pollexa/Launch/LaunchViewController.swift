//
//  LaunchViewController.swift
//  Pollexa
//
//  Created by admin on 14.05.2024.
//

import UIKit

protocol LaunchViewProtocol: AnyObject {
    func whenViewDidLoad()
}

final class LaunchViewController: BaseViewController, LaunchViewProtocol {
    
    lazy var viewModel : LaunchViewModelProtocol = {
        let viewModel = LaunchViewModel()
        viewModel.view = self
        return viewModel
    }()

    lazy var imageView : UIImageView = {
        let view = UIImageView(image: UIImage(named: "AppLogo"))
        view.contentMode = .scaleAspectFill
        view.alpha = 0
        return view
    }()
    
    lazy var titleLabel : UILabel = {
        let label = UILabel()
        label.textColor = UIColor(named: "textColor")
        label.text = "Pollexa"
        label.font = .init(name: "SF Pro", size: 36)
        label.font = .systemFont(ofSize: 36, weight: .init(700), width: .standard)
        label.textAlignment = .center
        label.alpha = 0
        return label
    }()
    

    // MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.viewDidLoad()
        navigationController?.navigationBar.isHidden = true
    }
    
    private func setupUI() {
        view.addSubview(imageView)
        view.addSubview(titleLabel)
    }
    
    private func configureConstraints(){
        imageView.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            make.centerY.equalToSuperview().inset(66)
            make.height.equalTo(132)
            make.width.equalTo(100)
        }
        
        titleLabel.snp.makeConstraints { make in
            make.top.equalTo(imageView.snp.bottom).offset(20)
            make.leading.trailing.equalToSuperview()
        }
        
        configureAnimation(position_true_false: false) { [unowned self] in
            goToDiscoverModule()
        }
    }
    
    func configureAnimation(position_true_false reversed:Bool, completion: (() -> Void)? = nil) {
        view.layoutIfNeeded()
        UIView.animate(withDuration: reversed ? 2 : 3, delay:reversed ? 0 : 0.2, usingSpringWithDamping: 0.6, initialSpringVelocity: 0.1) { [unowned self] in
            imageView.alpha = reversed ? 0.0 : 1.0
            titleLabel.alpha = reversed ? 0.0 : 1.0
        } completion: { [weak self] _ in
            completion?()
        }
    }
    
    func goToDiscoverModule() {
            self.configureAnimation(position_true_false: true) {
                if let windowScene = UIApplication.shared.connectedScenes.first as? UIWindowScene,
                   let keyWindow = windowScene.windows.first(where: { $0.isKeyWindow }) {
                    keyWindow.rootViewController = UINavigationController(rootViewController: DiscoverViewController())
                    keyWindow.makeKeyAndVisible()
                    self.dismiss(animated: true)
                } else {
                    // Diyelim ki windowScene e erişemedi
                    self.navigationController?.pushViewController(DiscoverViewController(), animated: true)
            }
        }
    }
}

extension LaunchViewController {
    
    func whenViewDidLoad() {
        setupUI()
        configureConstraints()
    }
}
