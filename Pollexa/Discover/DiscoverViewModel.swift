//
//  DiscoverViewModel.swift
//  Pollexa
//
//  Created by admin on 14.05.2024.
//
import Foundation

protocol DiscoverViewModelProtocol: AnyObject {
    var view: DiscoverViewProtocol? {get}
    func fetchPosts()
}

final class DiscoverViewModel: BaseViewModel, DiscoverViewModelProtocol {
    
    weak var view: DiscoverViewProtocol?

    func fetchPosts() {
        PostProvider.shared.fetchAll { [weak self] result in
            guard let self else {return}
            switch result {
            case .success(let posts):
                let list = posts.compactMap { PostList(post: $0) }
                self.view?.updateUI(list)
            case .failure(let failure):
                self.view?.showError(with: failure.localizedDescription)
            }
        }
    }

}
