//
//  DiscoverViewController.swift
//  Pollexa
//
//  Created by admin on 14.05.2024.
//

import UIKit

protocol DiscoverViewProtocol: AnyObject {
    func updateUI(_ posts:[PostList])
    func showError(with message:String)
}

final class DiscoverViewController: BaseViewController {
    var errorMessage : String = "" //normalde custom bi error view olur ve oradaki değerden test edilebilir fakat şimdilik bu değerde test ediyorum dediğim gibi baya yoğunum DiscoveryViewControllerTests line 36
    lazy var viewModel : DiscoverViewModelProtocol = {
        let viewModel = DiscoverViewModel()
        viewModel.view = self
        return viewModel
    }()
    
    var navStatusHeight : CGFloat {
        let navBarHeight = (navigationController?.navigationBar.frame.height) ?? 0
        return UIView.statusBarHeight + navBarHeight
    }
    
    var postList:[PostList]?

    lazy var avatarView : UIImageView = {
        let view = UIImageView(image: UIImage(named: "avatar_1"))
        view.contentMode = .scaleAspectFit
        view.clipsToBounds = true
        return view
    }()
    
    lazy var pollView : UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(hex: "#5856D6")
        view.layer.cornerRadius = 16
        return view
    }()
    
    private lazy var pollHeaderLabel : UILabel = {
        let label = UILabel()
        label.textAlignment = .left
        label.numberOfLines = 0
        return label
    }()
    
    private lazy var pollButton : UIButton = {
        let button = UIButton(type: UIButton.ButtonType.custom)
        button.backgroundColor = .white
        button.layer.cornerRadius = 6
        button.setImage(UIImage(systemName: "arrowshape.right.fill"), for: .normal)
        return button
    }()
    
    lazy var collectionView : UICollectionView = {
        let flowLayout = UICollectionViewFlowLayout()
        let cv = UICollectionView(frame: .zero, collectionViewLayout: flowLayout)
        cv.showsVerticalScrollIndicator = false
        cv.backgroundColor = .clear
        cv.delegate = self
        cv.dataSource = self
        cv.register(DiscoveryCell.self, forCellWithReuseIdentifier: DiscoveryCell.cellId)
        return cv
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
       
        configureNavigationController()
        setupUI()
        configureConstraints()
        
        viewModel.fetchPosts()
    }
    
    private func setupUI() {
        view.addSubview(pollView)
        view.addSubview(collectionView)
        pollView.addSubview(pollHeaderLabel)
        pollView.addSubview(pollButton)
    }
    
    private func configureConstraints(){
        avatarView.snp.makeConstraints { make in
            make.size.equalTo(34)
        }

        pollView.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview().inset(20)
            make.height.equalTo(UIView.HEIGHT * 0.084)
            make.top.equalToSuperview().inset(navStatusHeight + 20)
        }
        
        pollHeaderLabel.snp.makeConstraints { make in
            make.centerY.equalToSuperview()
            make.leading.equalToSuperview().inset(20)
        }
        
        pollButton.snp.makeConstraints { make in
            make.trailing.equalToSuperview().inset(20)
            make.centerY.equalToSuperview()
            make.size.equalTo(32)
        }
        
        collectionView.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview().inset(20)
            make.top.equalTo(pollView.snp.bottom).offset(20)
            make.bottom.equalToSuperview().inset(20)
        }
    }
    
    private func configureNavigationController(){
        navigationController?.navigationBar.prefersLargeTitles = true
        title = "Discover"

        let customIcon = UIBarButtonItem(customView: avatarView)
        let addButton = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(addAction))
        navigationItem.rightBarButtonItem = addButton
        navigationItem.leftBarButtonItem = customIcon
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        avatarView.layer.cornerRadius = avatarView.bounds.width / 4
    }

    @objc func addAction() {
        //
    }

}

extension DiscoverViewController : DiscoverViewProtocol {
    func showError(with message: String) {
        self.errorMessage = message
        self.showAlert(withMessage: message)
    }

    func updateUI(_ posts:[PostList]) {
        self.postList = posts
        let formattedString = NSMutableAttributedString(string: "\(posts.count) Active Polls \n", attributes: [
            .font: UIFont.systemFont(ofSize: 20,weight: .medium),
            .foregroundColor: UIColor.white
        ])

        let formattedString2 = NSMutableAttributedString(string: "See Details", attributes: [
            .font: UIFont.systemFont(ofSize: 16,weight: .medium),
            .foregroundColor: UIColor.white.withAlphaComponent(0.5)
        ])
        
        formattedString.append(formattedString2)

        pollHeaderLabel.attributedText = formattedString
        collectionView.reloadAsync()
    }
}

// MARK: - CollectionView DataSource Flow Layout
extension DiscoverViewController : UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return postList?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: DiscoveryCell.cellId, for: indexPath ) as? DiscoveryCell, let model = postList?[indexPath.row] {
            cell.delegate = self
            cell.index = indexPath.row
            cell.configureCell(model)
            return cell
        }
        return UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: UIView.HEIGHT * 0.419)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        20
    }
    
}

extension DiscoverViewController : DiscoverCellProtol {
    func didTapButton(_ index: Int) {
        postList?[index].voteCount += 1
        // tekrardan model olusturup sağ sol gorsele yüzde ve last voted logici ile çözülür hangi optiona tıklandıysa burdaki total sayidan yeni labelda gösterilir
        collectionView.reloadAsync()
    }
    
    
}
