//
//  DiscoveryCell.swift
//  Pollexa
//
//  Created by admin on 14.05.2024.
//

import UIKit

protocol DiscoverCellProtol : AnyObject {
    func didTapButton(_ index: Int)
}

final class DiscoveryCell: UICollectionViewCell {
    static let cellId:String = "DiscoveryCell"
    var index : Int?
    weak var delegate :DiscoverCellProtol?
    // MARK: - UIElements
    private lazy var avatarView : UIImageView = {
        let view = UIImageView()
        view.contentMode = .scaleAspectFit
        view.clipsToBounds = true
        return view
    }()
    
    private lazy var userNameLabel : UILabel = {
        let label = UILabel()
        label.textColor = UIColor(named: "textColor")
        label.font = .systemFont(ofSize: 17)
        label.textAlignment = .left
        return label
    }()
    
    private lazy var dateLabel : UILabel = {
        let label = UILabel()
        label.textColor = UIColor(hex: "#93A2B4")
        label.font = .systemFont(ofSize: 15)
        label.textAlignment = .right
        return label
    }()
    
    private lazy var seperatorView : UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(hex: "#78788033").withAlphaComponent(0.2)
        return view
    }()
    
    private lazy var lastVotedLabel : UILabel = {
        let label = UILabel()
        label.textColor = UIColor(hex: "#5B6D83")
        label.font = .systemFont(ofSize: 12)
        label.textAlignment = .left
        return label
    }()
    
    private lazy var contentLabel : UILabel = {
        let label = UILabel()
        label.textColor = UIColor(hex: "#02182B")
        label.font = .systemFont(ofSize: 17, weight: .medium)
        label.textAlignment = .left
        label.numberOfLines = 2
        return label
    }()
    
    private lazy var leftImage : UIImageView = {
        let view = UIImageView()
        view.contentMode = .scaleAspectFill
        view.clipsToBounds = true
        view.layer.cornerRadius = 10
        view.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMinXMinYCorner]
        return view
    }()
    
    private lazy var rightImage : UIImageView = {
        let view = UIImageView()
        view.contentMode = .scaleAspectFill
        view.clipsToBounds = true
        view.layer.cornerRadius = 10
        view.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMaxXMinYCorner]
        return view
    }()
    
    private lazy var stackView : UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .horizontal
        stackView.distribution = .fillEqually
        stackView.spacing = 4
        stackView.addArrangedSubview(leftImage)
        stackView.addArrangedSubview(rightImage)
        stackView.clipsToBounds = true
        return stackView
    }()
   
    private lazy var totalVoteLabel : UILabel = {
        let label = UILabel()
        label.textColor = UIColor(hex: "#93A2B4")
        label.font = .systemFont(ofSize: 15)
        label.textAlignment = .right
        label.isHidden = true
        return label
    }()
    
    lazy var leftLikeButton : UIButton = {
        let button = UIButton(type: .custom)
        button.backgroundColor = .white
        button.tag = 1
        button.addTarget(self, action: #selector(clickButton), for: .touchUpInside)
        button.setImage(UIImage(systemName: "hand.thumbsup.fill"), for: .normal)
        return button
    }()
    
    private lazy var rightLikeButton : UIButton = {
        let button = UIButton(type: .custom)
        button.backgroundColor = .white
        button.tag = 2
        button.addTarget(self, action: #selector(clickButton), for: .touchUpInside)
        button.setImage(UIImage(systemName: "hand.thumbsup.fill"), for: .normal)
        return button
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .white
        configureView()
        configureConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func hitTest(_ point: CGPoint, with event: UIEvent?) -> UIView? {
        guard self.point(inside: point, with: event) else { return nil }
        if self.leftLikeButton.point(inside: convert(point, to: leftLikeButton), with: event) {
            return self.leftLikeButton
        }
        if self.rightLikeButton.point(inside: convert(point, to: rightLikeButton), with: event) {
            return self.rightLikeButton
        }
        return super.hitTest(point, with: event)
    }
    
    private func configureView() {
        contentView.addSubview(avatarView)
        contentView.addSubview(userNameLabel)
        contentView.addSubview(dateLabel)
        contentView.addSubview(seperatorView)
        contentView.addSubview(lastVotedLabel)
        contentView.addSubview(contentLabel)
        contentView.addSubview(stackView)
        contentView.addSubview(totalVoteLabel)
        leftImage.addSubview(leftLikeButton)
        rightImage.addSubview(rightLikeButton)
    }
    
    private func configureConstraints() {
        let contentViewFrame = contentView.frame
        
        avatarView.snp.makeConstraints { make in
            make.leading.equalToSuperview().inset(20)
            make.size.equalTo(32)
            make.top.equalToSuperview().inset(20)
        }
        
        userNameLabel.snp.makeConstraints { make in
            make.leading.equalTo(avatarView.snp.trailing).offset(10)
            make.centerY.equalTo(avatarView.snp.centerY)
        }
        
        dateLabel.snp.makeConstraints { make in
            make.trailing.equalToSuperview().inset(20)
            make.centerY.equalTo(avatarView.snp.centerY)
        }
        
        seperatorView.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview().inset(20)
            make.top.equalTo(avatarView.snp.bottom).offset(12)
            make.height.equalTo(0.5)
        }
        
        lastVotedLabel.snp.makeConstraints { make in
            make.top.equalTo(seperatorView.snp.bottom).offset(12)
            make.leading.equalToSuperview().inset(20)
        }
        
        contentLabel.snp.makeConstraints { make in
            make.top.equalTo(lastVotedLabel.snp.bottom)
            make.leading.trailing.equalToSuperview().inset(20)
        }
        
        stackView.snp.makeConstraints { make in
            make.top.equalTo(contentLabel.snp.bottom).offset(16)
            make.trailing.leading.equalToSuperview().inset(20)
            make.height.equalTo(contentViewFrame.height * 0.4)
        }
        
        totalVoteLabel.snp.makeConstraints { make in
            make.top.equalTo(stackView.snp.bottom).offset(12)
            make.leading.equalToSuperview().inset(20)
            make.bottom.equalToSuperview()
        }
        
        leftLikeButton.snp.makeConstraints { make in
            make.leading.equalToSuperview().inset(10)
            make.bottom.equalToSuperview().inset(10)
            make.size.equalTo(30)
        }
        
        rightLikeButton.snp.makeConstraints { make in
            make.trailing.equalToSuperview().inset(10)
            make.bottom.equalToSuperview().inset(10)
            make.size.equalTo(30)
        }
    }
    
    func configureCell(_ model: PostList){
        userNameLabel.text = model.user.username
        avatarView.image = model.user.image
        dateLabel.text = DateFormatter().formattedTimeAgo(from: model.createdAt)
        lastVotedLabel.text = "test voted at"
        contentLabel.text = model.content
        
        if model.voteCount > 0 {
            totalVoteLabel.isHidden = false
            totalVoteLabel.text = "\(model.voteCount) Vote"
        }
        for option in model.options {
            // sag ve sol atamalari cok basit bi logic kontrol ediyorum normal bi projede tabiki boyle basit kontrol sorun cikarir farkindayim
            if option.id.contains("1") {
                leftImage.image = option.image
            } else if option.id.contains("2") {
                rightImage.image = option.image
            }
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        layer.cornerRadius = 20
        clipsToBounds = true
        avatarView.layer.cornerRadius = avatarView.frame.width / 2
        leftLikeButton.layer.cornerRadius = leftLikeButton.frame.width / 2
        rightLikeButton.layer.cornerRadius = rightLikeButton.frame.width / 2
    }
    
    @objc func clickButton(sender:UIButton) {
        if let delegate = delegate, let index = index {
            delegate.didTapButton(index)
        }
    }
}
