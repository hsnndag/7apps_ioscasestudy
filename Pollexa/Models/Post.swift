//
//  Post.swift
//  Pollexa
//
//  Created by Emirhan Erdogan on 13/05/2024.
//

import UIKit

struct Post: Decodable {
    
    // MARK: - Properties
    let id: String
    let createdAt: Date
    let content: String
    let options: [Option]
    let user : User
}


struct PostList: Decodable {
    // MARK: - Properties
    let id: String
    let createdAt: Date
    let content: String
    let options: [Option]
    let user : User
    var voteCount : Int = 0
    
    init(post : Post) {
        id = post.id
        createdAt = post.createdAt
        content = post.content
        options = post.options
        user = post.user
    }
}
