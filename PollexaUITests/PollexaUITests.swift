//
//  PollexaUITests.swift
//  PollexaUITests
//
//  Created by admin on 14.05.2024.
//

import XCTest

final class PollexaUITests: XCTestCase {
    
    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.

        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testDiscoveryUI() throws {
        // UI tests must launch the application that they test.
        
        let app = XCUIApplication()
        app.launch()

        app.navigationBars["Discover"].staticTexts["Discover"].tap()
        app.staticTexts["7 Active Polls \nSee Details"].tap()
        
        let collectionViewsQuery = app.collectionViews
        collectionViewsQuery/*@START_MENU_TOKEN@*/.images["post_1_option_1"]/*[[".cells.images[\"post_1_option_1\"]",".images[\"post_1_option_1\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.buttons["like"].tap()
        app.collectionViews.containing(.other, identifier:"Horizontal scroll bar, 1 page").element/*@START_MENU_TOKEN@*/.swipeRight()/*[[".swipeUp()",".swipeRight()"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/
        collectionViewsQuery/*@START_MENU_TOKEN@*/.images["post_1_option_2"]/*[[".cells.images[\"post_1_option_2\"]",".images[\"post_1_option_2\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.buttons["like"].tap()
    }
}
