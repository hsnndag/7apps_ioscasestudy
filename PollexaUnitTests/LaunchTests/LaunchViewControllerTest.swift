//
//  LaunchViewControllerTest.swift
//  PollexaUnitTests
//
//  Created by admin on 14.05.2024.
//

import XCTest
@testable import Pollexa

final class LaunchViewControllerTest: XCTestCase {
    var sut : LaunchViewController!
    var viewModel : SpyLaunchViewModel!
    
    override func setUpWithError() throws {
        sut = LaunchViewController()
        viewModel = SpyLaunchViewModel()
        sut.viewModel = viewModel
        viewModel.view = sut
    }

    override func tearDownWithError() throws {
        sut = nil
        viewModel = nil
    }
    

    func test_ViewModel_is_SettedAndLoaded() throws {
        XCTAssertTrue(viewModel.invokedViewSetter)
        _ = sut.view
        XCTAssertTrue(viewModel.invokedViewDidLoad)
    }

    func test_LaunchView_Animations_willShown() throws {
        XCTAssertEqual(sut.imageView.alpha, 0)
        XCTAssertEqual(sut.titleLabel.alpha, 0)

        let expectation = expectation(description: "Animation after alpha")
        sut.configureAnimation(position_true_false: false)
        DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
            XCTAssertEqual(self.sut.imageView.alpha, 1)
            XCTAssertEqual(self.sut.titleLabel.alpha, 1)
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 3.1)
    }
    
    func test_LaunchView_Animations_willHide() throws {
        let expectation = expectation(description: "Animation after alpha")
        sut.configureAnimation(position_true_false: true)
        DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
            XCTAssertEqual(self.sut.imageView.alpha, 0)
            XCTAssertEqual(self.sut.titleLabel.alpha, 0)
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 3.1)
    }
    
    func test_LaunchView_goToDiscoverModule() throws {
        let window = UIApplication.shared.connectedScenes.first as! UIWindowScene

        let expectation = expectation(description: "routing")
        sut.goToDiscoverModule()
        DispatchQueue.main.asyncAfter(deadline: .now() + 3.2) {
            XCTAssertTrue(window.keyWindow?.rootViewController?.children.first is DiscoverViewController)
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 3.3)
    }
}
