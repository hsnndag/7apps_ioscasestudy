//
//  SpyLaunchViewModel.swift
//  PollexaUnitTests
//
//  Created by admin on 14.05.2024.
//

import Foundation
@testable import Pollexa

final class SpyLaunchViewModel : LaunchViewModelProtocol {
    var invokedViewSetter = false
    var invokedViewSetterCount = 0
    var invokedView: LaunchViewProtocol?
    var invokedViewList = [LaunchViewProtocol?]()
    var invokedViewGetter = false
    var invokedViewGetterCount = 0
    var stubbedView: LaunchViewProtocol!

    var view: LaunchViewProtocol? {
        set {
            invokedViewSetter = true
            invokedViewSetterCount += 1
            invokedView = newValue
            invokedViewList.append(newValue)
        }
        get {
            invokedViewGetter = true
            invokedViewGetterCount += 1
            return stubbedView
        }
    }

    var invokedViewDidLoad = false
    var invokedViewDidLoadCount = 0

    func viewDidLoad() {
        invokedViewDidLoad = true
        invokedViewDidLoadCount += 1
    }
}
