//
//  SpyDiscoveryViewModel.swift
//  PollexaUnitTests
//
//  Created by admin on 16.05.2024.
//

import Foundation
@testable import Pollexa

final class SpyDiscoveryViewModel : DiscoverViewModelProtocol {
    var view: (any Pollexa.DiscoverViewProtocol)?
        
    var invokedFetchPosts = false
    var invokedFetchPostsCount = 0
    
    var failCase = false
    
    func fetchPosts() {
        invokedFetchPosts = true
        if !failCase {
            invokedFetchPostsCount += 1
            view?.updateUI(getModel())
        } else {
            view?.showError(with: "Test Error")
        }
    }
    
    func getModel() -> [PostList] {
        guard let fileUrl = Bundle.main.url(
            forResource: "posts",
            withExtension: "json"
        ) else {
            return []
        }
        
        guard let postData = try? Data.init(contentsOf: fileUrl) else {
            return []
        }
        
        do {
            let decoder = JSONDecoder()
            
            decoder.keyDecodingStrategy = .convertFromSnakeCase
            decoder.dateDecodingStrategy = .iso8601
            let posts = try decoder.decode([Post].self, from: postData)
            return posts.compactMap { PostList(post: $0) }
        } catch {
            return []
        }
    }
}
