//
//  DiscoveryViewControllerTests.swift
//  PollexaUnitTests
//
//  Created by admin on 16.05.2024.
//

import XCTest
@testable import Pollexa
// normalde viewprotokol da test edilebilir fakat ben burada viewmodeli spy yapıp caseleri test ettim
final class DiscoverViewControllerTests: XCTestCase {
    var sut : DiscoverViewController!
    var viewModel : SpyDiscoveryViewModel!
    
    override func setUpWithError() throws {
        sut = DiscoverViewController()
        viewModel = SpyDiscoveryViewModel()
        sut.viewModel = viewModel
        viewModel.view = sut
    }

    override func tearDownWithError() throws {
        sut = nil
        viewModel = nil
    }

    func test_ViewModelDidFetchPosts() throws {
        _ = sut.view
        XCTAssertTrue(viewModel.invokedFetchPosts)
    }
    
    func test_ViewModelDidFetchPosts_withFail() throws {
        viewModel.failCase = true
        _ = sut.view
        
        XCTAssertEqual(sut.errorMessage,"Test Error")
    }
    
    func test_CollectionView_NumberOfItems() throws {
        _ = sut.view
        let items = sut.collectionView.numberOfItems(inSection: 0)
        XCTAssertEqual(viewModel.getModel().count, items)
    }
    
    func test_CollectionViewCell_DidTappedLike() throws {
        _ = sut.view

        let cell = sut.collectionView.dequeueReusableCell(withReuseIdentifier: DiscoveryCell.cellId, for: IndexPath(row: 1, section: 0)) as! DiscoveryCell
        cell.index = 1
        cell.delegate = sut
        let expectation = expectation(description: "cell did click")
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            cell.leftLikeButton.sendActions(for: .touchUpInside)
            XCTAssertEqual(self.sut.postList?[1].voteCount, 1)
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 1.2)

    }

}
